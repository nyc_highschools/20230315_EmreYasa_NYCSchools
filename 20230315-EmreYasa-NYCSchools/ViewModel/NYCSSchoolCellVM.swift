//
//  NYCSSchoolCellVM.swift
//  20230315-EmreYasa-NYCSchools
//
//  Created by Emre Yasa on 3/15/23.
//

import Foundation

class NYCSSchoolCellVM {
    let title:String?
    let idSchool:String
    
    //Dependency Injection
    //To avoid coupling the ViewModel with the model, the NYCSSchoolCellVM class is created with the Schools object being passed in as a parameter. This makes the ViewModel independent of the data source, which allows it to be more easily tested and reused in different contexts.
    init(schools: Schools) {
        self.title = schools.name
        self.idSchool = schools.idSchool
    }
}
