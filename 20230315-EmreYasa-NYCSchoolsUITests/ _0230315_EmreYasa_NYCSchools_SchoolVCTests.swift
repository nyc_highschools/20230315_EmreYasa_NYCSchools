//
//  SchoolVCTests.swift
//  20230315-EmreYasa-NYCSchoolsTests
//
//  Created by Emre Yasa on 3/17/23.
//  UI Test
//

import XCTest

class  _0230315_EmreYasa_NYCSchools_SchoolVCTests: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launch()
    }
    
    //Test if table view exists, starts with no cell, waits for the API to fetch school data, reload the data, and then checks that the table view has at least one cell. The test also scrolls table view, selects the first cell, and checks the SchoolDetailVC is shown.
    func testTableView() {
        let tableView = app.tables.element(boundBy: 0)
        XCTAssertTrue(tableView.exists)
        XCTAssertEqual(tableView.cells.count, 0)
        
        // Wait for API to fetch data and reload table view
        let predicate = NSPredicate(format: "count > 0")
        let expectation = XCTNSPredicateExpectation(predicate: predicate, object: tableView.cells)
        let result = XCTWaiter.wait(for: [expectation], timeout: 10)
        XCTAssertEqual(result, .completed)
        
        // Test scrolling
        let start = tableView.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 0))
        let end = tableView.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 1))
        start.press(forDuration: 0, thenDragTo: end)
        
        // Test selecting a cell
        let cell = tableView.cells.element(boundBy: 0)
        XCTAssertTrue(cell.exists)
        cell.tap()
        
        // Test detail view controller
        let detailView = app.navigationBars["Liberation Diploma Plus"].exists
        XCTAssertTrue(detailView)
    }
    
    //Test if the search button exits, taps on it, enters a search query for a specific school, taps the search button, waits for the API to fetch school data, and reload table view and then waits at least one cell is shown
    func testSearch() {
        let searchButton = app.navigationBars["NYC High Schools"].buttons["Search"]
        XCTAssertTrue(searchButton.exists)
        searchButton.tap()
        
        let searchBar = app.searchFields.firstMatch
        XCTAssertTrue(searchBar.exists)
        searchBar.tap()
        searchBar.typeText("Liberation Diploma Plus")
        app.buttons["Search"].tap()
        
        // Wait for API to fetch data and reload table view
        let tableView = app.tables.element(boundBy: 0)
        let predicate = NSPredicate(format: "count > 0")
        let expectation = XCTNSPredicateExpectation(predicate: predicate, object: tableView.cells)
        let result = XCTWaiter.wait(for: [expectation], timeout: 10)
        XCTAssertEqual(result, .completed)
        
        // Test searching
        let cell = tableView.cells.element(boundBy: 0)
        XCTAssertTrue(cell.exists)
    }
}
