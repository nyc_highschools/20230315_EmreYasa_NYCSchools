//
//  20230315-EmreYasa-NYCSchools_SchoolDetailVC.swift
//  20230315-EmreYasa-NYCSchoolsTests
//
//  Created by Emre Yasa on 3/17/23.
//

import XCTest
@testable import _0230315_EmreYasa_NYCSchools

final class  _0230315_EmreYasa_NYCSchools_SchoolDetailVC: XCTestCase {
    
    //Test school detail view if it's correctly added to its view hierarchy
    func testDetailViewAddedToHierarchy() {
        let sut = SchoolDetailVC()
        sut.loadViewIfNeeded()
        XCTAssertTrue(sut.view.subviews.contains(sut.detailView))
    }
    
}
