//
//  APICallerTests.swift
//  20230315-EmreYasa-NYCSchoolsTests
//
//  Created by Emre Yasa on 3/17/23.
//

import XCTest
@testable import _0230315_EmreYasa_NYCSchools

class _0230315_EmreYasa_NYCSchools_APICallerTests: XCTestCase {
    
    var apiCaller: APICaller!
    
    override func setUp() {
        super.setUp()
        apiCaller = APICaller.shared
    }
    
    override func tearDown() {
        apiCaller = nil
        super.tearDown()
    }
    
    //Test if schools fetched successfully
    func testFetchSchools() {
        let expectation = self.expectation(description: "Fetching schools data")
        
        apiCaller.fetchSchools { result in
            switch result {
            case .success(let schools):
                XCTAssertNotNil(schools, "schools should not be nil")
                XCTAssertGreaterThan(schools.count, 0, "schools should not be empty")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Error fetching schools data: \(error.localizedDescription)")
            }
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    //Search for schools data from an API with given query, check if the result is accomplished
    func testSearchedSchools() {
        let expectation = self.expectation(description: "Searching schools data")
        
        let query = "School"
        apiCaller.searchedSchools(with: query) { result in
            switch result {
            case .success(let schools):
                XCTAssertNotNil(schools, "schools should not be nil")
                XCTAssertGreaterThan(schools.count, 0, "schools should not be empty")
                for school in schools {
                    XCTAssertTrue(((school.name?.lowercased().contains(query.lowercased())) != nil), "school name should contain query")
                }
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Error searching schools data: \(error.localizedDescription)")
            }
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    //Test if school are fetched with given school id
    func testFetchSchool() {
        let expectation = self.expectation(description: "Fetching school detail")
        
        let dbn = "21K690"
        apiCaller.fetchSchool(for: dbn) { result in
            switch result {
            case .success(let schoolDetail):
                XCTAssertNotNil(schoolDetail, "school detail should not be nil")
                XCTAssertEqual(schoolDetail.count, 1, "school detail should contain only one item")
                XCTAssertEqual(schoolDetail.first?.id, dbn, "school detail should match the specified dbn")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Error fetching school detail: \(error.localizedDescription)")
            }
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
}
