## What app does is below
- [x] Fetch the data from API and populate data to TableView
- [x] Navigate to selected schools to show SAT Scores of each schools
- [x] Show prompt if selected school does not have data
- [x] Search schools by name on main screen


## Features

- [x] Use UIKit
- [x] Use Swift
- [x] Show spinning indicator while school data is loaded in School SAT scores screen
- [x] Cached the fetched schools data for main screen for better UX
- [x] Add Unit Test and UI Test
- [x] Use dependency injection
- [x] Use MVVM architecture

